import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { observable } from 'rxjs';

@Component({
  selector: 'app-patientregistration',
  templateUrl: './patientregistration.component.html',
  styleUrls: ['./patientregistration.component.css']
})
export class PatientregistrationComponent implements OnInit {

  constructor(private router: Router,private hc:HttpClient) { }

  ngOnInit() {
  }

  patientReg(data){
    this.hc.post('nav/register/patient',data).subscribe((res)=>{
      if(res['message']=="null value not inserted"){
        alert("please fill the required fields");
        }
      
      else if(res["message"]=="registered successfully")
      {
        alert(res["message"])
        this.router.navigate(['nav/login/'])
      }
      else if(res["message"]=="name exists")
      {
        alert('name already exists please take another name')
      }
    })
   
  }

}
